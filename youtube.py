import json
import time

import requests
import dateutil.parser
from flask import Flask, render_template
from werkzeug.contrib.cache import SimpleCache

app = Flask(__name__)
cache = SimpleCache()

CHANNELS = (
    'TotalHalibut',
    'BlueXephos',
    'PressHeartToContinue',
    'Polaris',
    'yogscast2',
    'YogscastSips',
    'yogscastkim',
    'OMFGCata',
    'AngryJoeShow',
    'ForceSC2strategy',
    'PurgeGamers',
    'quill18',
    'arumba07',
    'shenryyr2',
    'ZiggyDStarcraft',
    'MathasGames',
    'grindinggear',
    'numberphile',
    'Computerphile',
    'TradeChat',
    'mikepreachwow',
    'BellularGaming',
)


def fetch_youtube_data(channel):
    '''
    Fetches youtube data for the given channel, returning a dictlist.
    '''
    resp = requests.get(
        'https://gdata.youtube.com/feeds/api/videos?'
        'author=%s&v=2&orderby=updated&alt=jsonc' % channel)
    resp.raise_for_status()

    data = resp.json()

    # Do some postprocessing, most notably on timestamps.
    result = []
    for item in data['data']['items']:
        item['updated'] = dateutil.parser.parse(item['updated'])
        item['uploaded'] = dateutil.parser.parse(item['uploaded'])
        item['duration'] = time.strftime(
            '%M:%S', time.gmtime(item['duration']))
        item['viewCount'] = ('{0:,}'.format(int(item['viewCount']))
                             if 'viewCount' in item else None)
        result.append(item)
    return result


def fetch_all_youtube_data():
    '''
    Fetches all youtube data, from the `CHANNELS` defined.
    '''
    result = []
    for channel in CHANNELS:
        result.extend(fetch_youtube_data(channel))
    return sorted(result, key=lambda e: e['uploaded'], reverse=True)


def fetch_cached_data():
    '''
    Fetches cached data, if none exists fetches new data and refreshes the
    cache.
    '''
    data = cache.get('data')
    if data is None:
        data = fetch_all_youtube_data()
        cache.set('data', data, timeout=60 * 60)
    return data


@app.route('/')
def index():
    return render_template(
        'index.html',
        data=fetch_cached_data(),
        channels=CHANNELS,
    )

if __name__ == '__main__':
    app.run(debug=True)
